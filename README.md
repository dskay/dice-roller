# Dice App

This repository contains the source code for my Dice app. This mobile app randomly rolls dices.

## Purpose

The purpose of this app is to allow people to random roll dices virtually in the event a physical device is not available.

## Demo

![Dice demo](/images/dice.png)

